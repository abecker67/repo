package com.abecker.carouselapp.provider;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.abecker.carouselapp.R;

public class ImagesProvider {
	
	public static Resources res;

	public static int[] ids = new int[] { R.drawable.f0, R.drawable.f1, R.drawable.f2, R.drawable.f3, R.drawable.f4, R.drawable.f5, R.drawable.f6, R.drawable.f7, R.drawable.f8, R.drawable.f9 };
	public static Drawable[] drawables;
	
	private static Drawable idToDrawable(int id){
		return res.getDrawable(id);
	}
	
	public static void initDrawables(Resources res){
		ImagesProvider.res = res;
		drawables = new Drawable[]{
					idToDrawable(R.drawable.f0),
					idToDrawable(R.drawable.f1),
					idToDrawable(R.drawable.f2),
					idToDrawable(R.drawable.f3),
					idToDrawable(R.drawable.f4),
					idToDrawable(R.drawable.f5),
					idToDrawable(R.drawable.f6),
					idToDrawable(R.drawable.f7),
					idToDrawable(R.drawable.f8),
					idToDrawable(R.drawable.f9)
				};
	}
}
