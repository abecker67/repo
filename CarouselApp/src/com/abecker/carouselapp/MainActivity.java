package com.abecker.carouselapp;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.abecker.androidutils.ui.CarouselLayout;
import com.abecker.androidutils.ui.CarouselLayout.ChildStaticTransformationDatasource;
import com.abecker.carouselapp.adapter.CarouselAdapter;

public class MainActivity extends Activity implements OnItemClickListener, OnItemSelectedListener, ChildStaticTransformationDatasource {

	TextView txtView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		txtView = (TextView)findViewById(R.id.textview);
		CarouselLayout carousel = (CarouselLayout) findViewById(R.id.carousel);
		carousel.setChildHeight(200);
		carousel.setChildWidth(200);
		carousel.setChildrenLimit(3);
		carousel.setOnItemClickListener(this);
		carousel.setOnItemSelectedListener(this);
		carousel.setChildStaticTransformationDatasource(this);
		carousel.setBackgroundColor(Color.parseColor("#809080FF"));
		carousel.setAnimateToCenter(false);
		carousel.setAdapter(new CarouselAdapter(this));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		txtView.setText(""+arg2);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.d(getClass().getName(), "Click " + arg2);
	}

	@Override
	public Transformation getChildStaticTransformation(ViewGroup parent, View child, float distanceFromCenter) {
		Transformation t = new Transformation();
		float sign = 0;
		if (!(-1 < distanceFromCenter && distanceFromCenter < 1))
			sign = distanceFromCenter / Math.abs(distanceFromCenter);
		float scale = 1 - Math.abs((float) distanceFromCenter / parent.getWidth());
		t.clear();
		t.setTransformationType(Transformation.TYPE_MATRIX);

		Matrix mMatrix = t.getMatrix();
		float s = 1f - (1f-scale)/2f;
		mMatrix.setScale(s, s, child.getWidth() / 2, child.getHeight() / 2);
		mMatrix.postRotate((1 - scale) * 45 * sign, child.getWidth() / 2, child.getHeight() / 2);

		return t;
	}
}
