package com.abecker.carouselapp.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.abecker.carouselapp.provider.ImagesProvider;

public class CarouselAdapter extends BaseAdapter {

	Context context;

	public CarouselAdapter(Context context) {
		this.context = context;
		ImagesProvider.initDrawables(context.getResources());
	}

	@Override
	public int getCount() {
		return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView view = null;
		if (convertView != null)
			view = (ImageView) convertView;
		else
			view = new ImageView(context);
		view.setImageDrawable(ImagesProvider.drawables[position]);
		return view;
	}

}
